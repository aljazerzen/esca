<?php

return [
	'delivery.status.0' => 'Accepting requests',
	'delivery.status.1' => 'Confirmed',
	'delivery.status.2' => 'Completed',
	'delivery.status.3' => 'Limit unreached',
	'delivery.status.4' => 'Rejected',
	'request.status.0' => 'Awaiting conformation',
	'request.status.1' => 'Confirmed',
	'request.status.2' => 'Delivered',
	'request.status.3' => 'Awaiting conformation',
	'request.status.4' => 'Rejected',
	'user.type.0' => 'Customer',
	'user.type.1' => 'Provider',
	'setting.appname' => 'Application name',
	'setting.request_regular_handle_time_before_assess' => 'Hours before delivery closure to handle regular requests',
	'post.type.0' => 'Public',
	'post.type.1' => 'Logged-in customers',
	'post.type.2' => 'Regular customers',
	'post.type.3' => 'Providers',
];