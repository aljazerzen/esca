@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>{{ $post->title }}
                        </h3></div>

                    <div class="panel-body">
                        {!! $post->text !!}

                        @include('vendor.photogallery', ['photos' => $post->photos])

                    </div>

                    <div class="panel-footer">
                        Posted {{ $post->created_at }} by {{ $post->user->getPublicName() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection