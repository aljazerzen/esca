@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Orders</div>

            @if (!(Auth::user()->requests->isEmpty()))
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Amount</th>
                        <th>Delivery date</th>
                        <th>Date of request</th>
                        <th>Status</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(Auth::user()->requests as $request)
                        <tr>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->amount . ' ' . $request->product->unitObj->name }}</td>
                            <td>{{ $request->delivery->date->format(\App\Setting::get('date_format')) }}</td>
                            <td>{{ $request->created_at->format(\App\Setting::get('datetime_format')) }}</td>
                            <td><span class="label label-{{ $request->delivery->status < 3 ? 'success' : ($request->delivery->status == 4 ? 'danger' : 'warning') }}">{{ trans('general.request.status.' . $request->delivery->status) }}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    You have no orders jet. To order a product go to products page.
                </div>
            @endif
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Regular orders</div>

            @if (!(Auth::user()->requestsRegular->isEmpty()))
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Amount</th>
                        <th>Every</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(Auth::user()->requestsRegular as $request)
                        <tr>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->amount . ' ' . $request->product->unitObj->name }}</td>
                            <td>{{ $request->period }} week</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    You have no orders jet. To order a product go to products page.
                </div>
            @endif
        </div>
    </div>

@endsection
