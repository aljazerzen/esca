@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Profile</div>

            <div class="panel-body">
                <div class="col-md-9">
                    <form class="form-horizontal" action="{{ url('profile') }}" method="POST" id="profile-form">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control dataNew" name="name" value="{{ $user->name }}">
                                <p class="form-control-static dataOld">{{ $user->name }}</p>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>
                            <div class="col-md-8">
                                <input id="username" type="text" class="form-control dataNew" name="username" value="{{ $user->username }}">
                                <p class="form-control-static dataOld">{{ $user->username }}</p>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail address</label>
                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control dataNew" name="email" value="{{ $user->email }}">
                                <p class="form-control-static dataOld">{{ $user->email }}</p>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control dataNew" name="password">
                                <p class="form-control-static dataOld">******</p>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div id="password_confirmation_container" class="collapse collapse_edit">
                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password_confirmation" class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-8">
                                    <input id="password_confirmation" type="password" class="form-control dataNew" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>
                            <div class="col-md-8">
                                <input id="address" type="text" class="form-control dataNew" name="address" value="{{ $user->address }}">
                                <p class="form-control-static dataOld">{{ $user->address }}</p>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('addressPostNumber') ? ' has-error' : '' }}">
                            <label for="addressPostNumber" class="col-md-4 control-label">Post number</label>
                            <div class="col-md-8">
                                <input id="addressPostNumber" type="text" class="form-control dataNew" name="addressPostNumber" value="{{ $user->addressPostNumber }}">
                                <p class="form-control-static dataOld">{{ $user->addressPostNumber }}</p>

                                @if ($errors->has('addressPostNumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('addressPostNumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-3" id="actionBar">
                    <div class="collapse in collapse_edit">
                        <div class="btn btn-primary margin" onclick="editProfile()">Uredi</div>
                    </div>
                    <div class="collapse collapse_edit">
                        <div class="btn btn-primary margin" onclick="cancelEdit()">Prekliči</div>
                    </div>
                    <div class="collapse margin collapse_edit" onclick="saveProfile()">
                        <div class="btn btn-primary">Shrani</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        p.dataOld {
            margin-left: 13px;
        }

        .dataNew {
            display: none;
        }

        #actionBar .btn {
            width: 100%;
        }

        .margin {
            margin-bottom: 20px;
        }

    </style>
@endsection

@section('scripts')
    <script>
        $('.dataNew').hide();

        function editProfile() {
            $('.dataOld').hide();
            $('.dataNew').show();
            $('.collapse_edit').collapse("toggle");
        }

        function cancelEdit() {
            $('.dataOld').show();
            $('.dataNew').hide();
            $('.collapse_edit').collapse("toggle");
        }

        function saveProfile() {
            $("#profile-form").submit();
        }
    </script>
@endsection