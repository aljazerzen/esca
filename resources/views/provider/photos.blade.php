@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Photos</div>

            @if (\App\Photo::count() > 0)
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Added</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(\App\Photo::all() as $photo)
                        <tr data-url="{{ $photo->url }}">
                            <td><img src="{{ $photo->url }}" height="50"/></td>
                            <td>{{ $photo->name }}</td>
                            <td>{{ $photo->created_at }}</td>
                            <td class="action-td">
                                <div>
                                    <form role="form" method="POST" action="{{ url('/provider/photo/' . $photo->id) }}" class="form-inline collapse" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <div class="fileinput-new input-group input-group-sm{{ $errors->has('text') ? ' has-error' : '' }}" data-provides="fileinput">
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name"/>
                                                <div class="input-group-addon" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="photo"></span>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>

                                                @if ($errors->has('text'))
                                                    <span class="help-block">
                                    <strong>{{ $errors->first('text') }}</strong>
                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                    </form>
                                </div>
                                <div>
                                    <button class="btn btn-sm btn-primary edit-btn">Edit</button>
                                </div>
                                <a href="{{ url('provider/photo/' . $photo->id . '/delete') }}" class="btn btn-sm btn-primary">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    Hmmm, there are no photos jet.
                </div>
            @endif
            <div class="panel-footer">
                <form role="form" method="POST" action="{{ url('/provider/photo') }}" class="form-inline" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="fileinput-new input-group {{ $errors->has('text') ? ' has-error' : '' }}" data-provides="fileinput">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name"/>
                            <div class="input-group-addon" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="photo"></span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>

                            @if ($errors->has('text'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('text') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/jasny-bootstrap.min.css') }}">
    <style>
        .action-td>* {
            display: inline-block;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('js/jasny-bootstrap.min.js') }}"></script>
    <script>
        $().ready(function () {
            $(".edit-btn").click(function () {
                $(this).hide();
                $(this).parent().prev().children().first().show();
            });
        });
    </script>
@endpush