@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Delivery</div>
            <div class="panel-body">
                <form role="form" method="POST" action="{{ url('/provider/delivery/'. $delivery->id) }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        <label for="date">Date</label>

                        <input id="date" class="form-control" data-provide="datepicker" data-date-format="dd.mm.yyyy" name="date" value="{{ $delivery->date->format(\App\Setting::get('date_format')) }}">

                        @if ($errors->has('date'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('closes_at') ? ' has-error' : '' }}">
                        <label for="closes_at">Accept requests until</label>

                        <input id="closes_at" class="form-control" data-provide="datepicker" data-date-format="dd.mm.yyyy" name="closes_at" value="{{ $delivery->closes_at->format(\App\Setting::get('datetime_format')) }}">

                        @if ($errors->has('closes_at'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('closes_at') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label for="price">Price</label>

                        <div class="input-group">
                            <input id="price" type="number" step='0.01' placeholder='0.00' class="form-control" name="price"
                                   value="{{ $delivery->price }}">
                            <span class="input-group-addon">€</span>
                        </div>

                        @if ($errors->has('price'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('limit') || $errors->has('limit_type') ? ' has-error' : '' }}">
                        <label for="unit">Limit</label>

                        <div class="input-group">
                            <div class="input-group-addon"> At least</div>
                            <input id="limit" type="number" step='1' min='1' class="form-control" name="limit" style="width:60%;"
                                   value="{{ $delivery->limit }}"/>
                            <select id="limit_type" class="form-control" name="limit_type" style="width:40%;">
                                <option value="0"{{ $delivery->type==0 ? " selected" : "" }}>requests</option>
                                <option value="1"{{ $delivery->type==1 ? " selected" : "" }}>products</option>
                            </select>
                        </div>

                        @if ($errors->has('limit'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('limit') }}</strong>
                                </span>
                        @endif

                        @if ($errors->has('limit_type'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('limit_type') }}</strong>
                                </span>
                        @endif
                    </div>


                    <input type="submit" class="btn btn-primary" value="Save"/>

                </form>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
@endpush