@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Posts
                <div class="pull-right" data-toggle="modal" data-target="#add-post-modal">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
            </div>

            @if (count($posts) > 0)
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Posted</th>
                        <th>Author</th>
                        <th>Visibility</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td>{{ $post->user->getPublicName() }}</td>
                            <td><span class="label label-default">{{ trans('general.post.type.'.$post->type) }}</span></td>
                            <td>
                                <a href="{{ url('provider/post/' . $post->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                <a href="{{ url('provider/post/' . $post->id . '/delete') }}" class="btn btn-sm btn-primary">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    Hmmm, there are no posts jet.
                </div>
            @endif
        </div>
    </div>

    <div class="modal fade" id="add-post-modal" tabindex="-1" role="dialog"
         aria-labelledby="add-post-modal-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" method="POST" action="{{ url('/provider/post') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="add-post-modal-title">New post</h4>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}
                        <input type="hidden" name="author" value="{{ Auth::user()->id }}">

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">

                            @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                            <label for="text" class="col-md-4 control-label">Text</label>

                            @include('vendor.texteditor', ['name' => 'text', 'text' => old('text')])


                        @if ($errors->has('text'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail btn-photo" style="text-align: center;">
                                    <img src=''/>
                                    <input type="hidden" onclick="$(this).parent().parent().remove();">
                                    <span>Add photo</span>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Visibility</label>

                            <select id="type" name="type" class="form-control">
                                <option value="0" {{ $post->type == 0 ? 'selected' : '' }}>{{ trans('general.post.type.0') }}</option>
                                <option value="1" {{ $post->type == 1 ? 'selected' : '' }}>{{ trans('general.post.type.1') }}</option>
                                <option value="3" {{ $post->type == 3 ? 'selected' : '' }}>{{ trans('general.post.type.3') }}</option>
                            </select>

                            @if ($errors->has('type'))
                                <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Post"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('snippet.photoselector')

@endsection