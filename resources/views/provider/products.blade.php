@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Products
                <div class="pull-right" data-toggle="modal" data-target="#add-product-modal">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
            </div>

            @if (count($products) > 0)
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Added</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Requested</th>
                        <th>Available</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->created_at }}</td>
                            <td>{{ $product->pricePerUnit . "€/" . $product->unitObj->name }}</td>
                            <td>{{ $product->stock }}</td>
                            <td>{{ $product->getRequestedStock() . " (" . $product->getPercentRequested() . "%)" }}</td>
                            <td>{{ $product->getAvailableStock() . " (" . $product->getPercentAvailable() . "%)" }}</td>
                            <td>
                                <a href="{{ url('provider/product/' . $product->id) }}" class="btn btn-sm btn-primary">Edit product</a>
                                <a href="{{ url('provider/product/' . $product->id) . '/delete' }}" class="btn btn-sm btn-primary">Delete</a>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#edit-stock-modal" data-stock="{{$product->stock}}" data-id="{{ $product->id }}">Stock</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    Hmmm, there are no products jet.
                </div>
            @endif
        </div>
    </div>

    <div class="modal fade" id="edit-stock-modal" tabindex="-1" role="dialog"
         aria-labelledby="edit-stock-modal-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" method="POST" action="{{ url('/provider/product/') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="edit-stock-modal-title">Edit stock</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="author" value="{{ Auth::user()->id }}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">New stock</label>

                            <div class="input-group" id="stock-button-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" data-amount="-10" type="button">-10</button>
                                    <button class="btn btn-default" data-amount="-1" type="button">-1</button>
                                </span>
                                <input id="stock" type="number" class="form-control" min="0" name="stock" value="{{ old('stock') }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" data-amount="+1" type="button">+1</button>
                                    <button class="btn btn-default" data-amount="+10" type="button">+10</button>
                                </span>
                            </div>

                            @if ($errors->has('stock'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('stock') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Update"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-product-modal" tabindex="-1" role="dialog"
         aria-labelledby="add-product-modal-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" method="POST" action="{{ url('/provider/product') }}" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="add-product-modal-title">New product</h4>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}
                        <input type="hidden" name="author" value="{{ Auth::user()->id }}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name</label>

                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Description</label>

                            <textarea id="description" type="text" class="form-control" name="description"
                                      style="resize: none;" rows="8">{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="thumbnail btn-thumbnail" style="text-align: center;">
                                <img src='' style="max-height: 100px"/>
                                <input type="hidden" name="thumbnail_id" value="{{ old('thumbnail_id') }}"/>
                                <span>Select thumbnail</span>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pricePerUnit') ? ' has-error' : '' }}">
                            <label for="pricePerUnit">Price per unit</label>

                            <input id="pricePerUnit" type="number" step='0.01' placeholder='0.00' class="form-control" name="pricePerUnit"
                                   value="{{ old('pricePerUnit') }}">

                            @if ($errors->has('pricePerUnit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pricePerUnit') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                            <label for="unit">Unit</label>

                            <select id="unit" class="form-control" name="unit">
                                @foreach(App\Unit::where('is_visible','=',1)->get() as $unit)
                                    <option value="{{ $unit->id }}"{{ $unit->id == old('unit') ? " selected" : ""}}>{{ $unit->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('unit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('unit') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Add"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('snippet.photoselector')

@endsection

@push('scripts')
    <script>
        $().ready(function () {
            $('#stock-button-group .btn').click(function () {
                $('#stock').val(parseInt($('#stock').val()) + parseInt($(this).data('amount')));
            });

            $('#edit-stock-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                console.log(button.data('stock'));
                $(this).find('#stock').val(button.data('stock'));
                $(this).find('form').attr('action', $(this).find('form').attr('action') + "/" + button.data('id') + "/stock");
            })
        });
    </script>
@endpush
