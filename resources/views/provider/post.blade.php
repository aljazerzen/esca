@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Edit post
            </div>

            <div class="panel-body">

                <form role="form" method="POST" action="{{ url('/provider/post/' . $post->id) }}">

                    {{ csrf_field() }}
                    <input type="hidden" id="edit-post-modal-author" name="author" value="{{ Auth::user()->id }}">

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>

                        <input id="title" type="text" class="form-control" name="title" value="{{ $post->title }}">

                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                        <label for="text" class="col-md-4 control-label">Text</label>

                        @include('vendor.texteditor', ['name' => 'text', 'text' => $post->text])

                        @if ($errors->has('text'))
                            <span class="help-block">
                                <strong>{{ $errors->first('text') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="row">
                            @foreach($post->photos as $photo)
                                <div class="col-md-2 col-sm-4">
                                    <div class="thumbnail btn-photo" style="text-align: center;">
                                        <img src='{{ $photo->url }}' onclick="$(this).parent().parent().remove();"/>
                                        <input type="hidden" name="photo[]" value="{{ $photo->id }}"/>
                                        <span>{{ $photo->name }}</span>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-md-2 col-sm-4">
                                <div class="thumbnail btn-photo" style="text-align: center;">
                                    <img src='' style="max-width: 100%"/>
                                    <input type="hidden" onclick="$(this).parent().parent().remove();">
                                    <span>Add photo</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="col-md-4 control-label">Visibility</label>

                        <select id="type" name="type" class="form-control">
                            <option value="0" {{ $post->type == 0 ? 'selected' : '' }}>{{ trans('general.post.type.0') }}</option>
                            <option value="1" {{ $post->type == 1 ? 'selected' : '' }}>{{ trans('general.post.type.1') }}</option>
                            <option value="3" {{ $post->type == 3 ? 'selected' : '' }}>{{ trans('general.post.type.3') }}</option>
                        </select>

                        @if ($errors->has('type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="pull-right">
                        <a href="{{ url('/provider/posts') }}" class="btn btn-default">Cancel</a>
                        <input type="submit" class="btn btn-primary" value="Update"/>
                    </div>
                </form>

            </div>

        </div>
    </div>

    @include('snippet.photoselector')

@endsection