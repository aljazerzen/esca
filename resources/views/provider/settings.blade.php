@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
                @if (\App\Setting::count() > 0)
                    <form method="post" action="{{ url('provider/settings') }}">
                        {{ csrf_field() }}

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Setting::all() as $setting)
                                <tr>
                                    <td>{{ trans('general.setting.' . $setting->key) }}</td>
                                    <td>
                                        <input type="text" class="form-control" name="{{ $setting->key }}" value="{{ $setting->value }}" \>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                @else
                    <div class="panel-body">No settings are defined.</div>
                @endif

            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Units</div>
                @if (App\Unit::count() > 0)
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <col width="10%">
                        <col width="60%">
                        <col width="30%">

                        @foreach(App\Unit::all() as $unit)
                            <tr>
                                <td>{{ $unit->id }}</td>
                                <td>
                                    <span class="unit-label">{{ $unit->name }}</span>
                                    <form class="form-inline" style="display:none" method="post" action="{{ url('provider/unit/' . $unit->id) }}">
                                        {{ csrf_field() }}
                                        <input name="name" class="form-control input-sm" value="{{ $unit->name }}" \>
                                        <input type="submit" class="btn btn-sm btn-primary" value="Save" \>
                                    </form>
                                </td>
                                <td>
                                    <form method="post" class="status-form" action="{{ url('provider/unit/' . $unit->id) }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="{{ $unit->name }}" \>
                                        <input type="hidden" name="is_visible" value="{{ $unit->is_visible ? "0" : "1"}}" \>

                                        <input type="submit" class="btn btn-sm btn-{{ $unit->is_visible ? "success" : "danger" }}"
                                               value="{{  $unit->is_visible ? "Shown" : "Hidden"}}">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="panel-body">No units are defined jet.</div>
                @endif
                <div class="panel-footer">
                    <form method="post" action="{{ url('provider/unit') }}" class="form-inline">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary" value="Add" \>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $().ready(function () {
            $('.unit-label').click(function () {
                $(this).hide();
                $(this).next().show();
            });
            $('.status-form').change(function () {
                $(this).submit();
            });
        });
    </script>
@endpush