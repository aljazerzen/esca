@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Deliveries
                <div class="pull-right" data-toggle="modal" data-target="#add-delivery-modal">
                    <span class="glyphicon glyphicon-plus"></span>
                </div>
            </div>
            @if (App\Delivery::count() > 0)
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Accepting orders until</th>
                        <th>Route</th>
                        <th>Price</th>
                        <th>Orders</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach(App\Delivery::all() as $delivery)
                        <tr>
                            <td>{{ $delivery->id }}</td>
                            <td>{{ $delivery->date->format(\App\Setting::get('date_format')) }}</td>
                            <td>{{ $delivery->closes_at->format(\App\Setting::get('datetime_format')) .
                                ($delivery->status == \App\Delivery::$STATUS_ACCEPTING ? "(" . (new \Carbon\Carbon())->diffInDays($delivery->closes_at) . " days left)" : '') }}</td>
                            <td>{{ $delivery->route }}</td>
                            <td>{{ $delivery->price . "€ per request"}}</td>
                            <td>{{ $delivery->getProgress() . " of " . $delivery->limit . ' ' . ($delivery->limit_type==0 ? "requests" : "products") . '(' . $delivery->getPercentageProgress() . '%)'  }}</td>
                            <td><span class="label label-{{ $delivery->status < 3 ? 'success' : ($delivery->status == 3 ? 'danger' : 'warning') }}">{{ trans('general.delivery.status.' . $delivery->status) }}</span></td>
                            <td>
                                @if($delivery->status == \App\Delivery::$STATUS_ACCEPTING)
                                    <a href="{{ url('provider/delivery/' . $delivery->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <a href="{{ url('provider/delivery/' . $delivery->id . '/delete') }}" class="btn btn-sm btn-primary">Delete</a>
                                @elseif($delivery->status == \App\Delivery::$STATUS_UNMET)
                                    <a href="{{ url('/provider/delivery/' . $delivery->id . "/setstatus?status=1") }}" class="btn btn-sm btn-primary">
                                        Confirm anyway</a>
                                    <a href="{{ url('/provider/delivery/' . $delivery->id . "/setstatus?status=4") }}" class="btn btn-sm btn-warning">
                                        Reject</a>
                                @elseif($delivery->status == \App\Delivery::$STATUS_CONFIRMED)
                                    <a href="{{ url('/provider/delivery/' . $delivery->id . "/setstatus?status=2") }}" class="btn btn-sm btn-warning">
                                        Set completed</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="panel-body">No deliveries are defined jet.</div>
            @endif

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Regular orders</div>

            @if (!(\App\RequestRegular::all()->isEmpty()))
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Product</th>
                        <th>Amount</th>
                        <th>Every</th>
                        <th>Last delivery</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach(\App\RequestRegular::all() as $request)
                        <tr>
                            <td>{{ $request->product->id }}</td>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->amount . ' ' . $request->product->unitObj->name }}</td>
                            <td>{{ $request->period }} week</td>
                            <td>{{ $request->lastDelivery ? $request->lastDelivery->date->format(\App\Setting::get('date_format')) : 'Never' }}</td>
                            <td>
                                {!! \App\Delivery::getUpcomingDelivery() && $request->needsThisDelivery(\App\Delivery::getUpcomingDelivery()) ? '<span class="label label-primary">Needs upcoming delivery</span>' : '' !!}
                                {!! $request->isLate() ? '<span class="label label-danger">Late</span>' : '' !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    You have no orders jet. To order a product go to products page.
                </div>
            @endif
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Routes</div>
            <div class="panel-body">
                WIP
                {{--<form method="post" action="{{ url('provider/delivery') }}">--}}
                {{--{{ csrf_field() }}--}}
                {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                {{--<label for="name">Name</label>--}}
                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">--}}

                {{--@if ($errors->has('name'))--}}
                {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('name') }}</strong>--}}
                {{--</span>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--<div class="pull-right">--}}
                {{--<input type="submit" class="btn btn-primary" value="Save" \>--}}
                {{--</div>--}}
                {{--</form>--}}
            </div>
        </div>

    </div>

    <div class="modal fade" id="add-delivery-modal" tabindex="-1" role="dialog"
         aria-labelledby="add-delivery-modal-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" method="POST" action="{{ url('/provider/delivery') }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="add-delivery-modal-title">New delivery</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date">Date</label>

                            <input id="date" class="form-control" data-provide="datepicker" data-date-format="dd.mm.yyyy" name="date" value="{{ old('date') }}">

                            @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('closes_at') ? ' has-error' : '' }}">
                            <label for="closes_at">Accept requests until</label>

                            <input id="closes_at" class="form-control" data-provide="datepicker" data-date-format="dd.mm.yyyy" name="closes_at" value="{{ old('closes_at') }}">

                            @if ($errors->has('closes_at'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('closes_at') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price">Price</label>

                            <div class="input-group">
                                <input id="price" type="number" step='0.01' placeholder='0.00' class="form-control" name="price"
                                       value="{{ old('price') }}">
                                <span class="input-group-addon">€</span>
                            </div>

                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('limit') || $errors->has('limit_type') ? ' has-error' : '' }}">
                            <label for="unit">Limit</label>

                            <div class="input-group">
                                <div class="input-group-addon"> At least</div>
                                <input id="limit" type="number" step='1' min='1' class="form-control" name="limit" style="width:60%;"
                                       value="{{ old('limit') }}"/>
                                <select id="limit_type" class="form-control" name="limit_type" style="width:40%;">
                                    <option value="0"{{ old('type')==0 ? " selected" : "" }}>requests</option>
                                    <option value="1"{{ old('type')==1 ? " selected" : "" }}>products</option>
                                </select>
                            </div>

                            @if ($errors->has('limit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('limit') }}</strong>
                                </span>
                            @endif

                            @if ($errors->has('limit_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('limit_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Add"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
@endpush