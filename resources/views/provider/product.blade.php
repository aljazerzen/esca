@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Edit product
            </div>

            <div class="panel-body">
                <form role="form" method="POST" action="{{ url('/provider/product/' . $product->id) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-md-6">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name</label>

                            <input id="name" type="text" class="form-control" name="name" value="{{ $product->name }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Description</label>

                            <textarea id="description" class="form-control" name="description"
                                      style="resize: none;" rows="8">{{ $product->description }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('pricePerUnit') ? ' has-error' : '' }}">
                            <label for="pricePerUnit">Price per unit</label>

                            <input id="pricePerUnit" type="number" step='0.01' placeholder='0.00' class="form-control" name="pricePerUnit"
                                   value="{{ $product->pricePerUnit }}">

                            @if ($errors->has('pricePerUnit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pricePerUnit') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                            <label for="unit">Unit</label>

                            <select id="unit" class="form-control" name="unit">
                                @foreach(App\Unit::getAllVisible() as $unit)
                                    <option value="{{ $unit->id }}"{{ $unit->id == $product->unit ? " selected" : ""}}>{{ $unit->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('unit'))
                                <span class="help-block">
                                <strong>{{ $errors->first('unit') }}</strong>
                            </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-primary" value="Save"/>
                    </div>
                    <div class="col-md-6" id="thumbnail-container">
                        <div class="col-sm-12">
                            <div class="thumbnail btn-thumbnail" style="text-align: center;">
                                <img id='thumbnail-preview' src='{{  $product->thumbnail ? $product->thumbnail->url : ''}}' style="max-width: 100%"/>
                                <input type="hidden" name="thumbnail_id" value="{{ $product->thumbnail ? $product->thumbnail->id : ''}}">
                                <span>{{ $product->thumbnail ? $product->thumbnail->name : 'Select thumbnail'}}</span>
                            </div>
                        </div>
                        @foreach($product->photos as $photo)
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail btn-photo" style="text-align: center;">
                                    <img src='{{ $photo->url }}' style="max-width: 100%" onclick="$(this).parent().parent().remove();"/>
                                    <input type="hidden" name="photo[]" value="{{ $photo->id }}"/>
                                    <span>{{ $photo->name }}</span>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-3 col-sm-6">

                            <div class="thumbnail btn-photo" style="text-align: center;">
                                <img src='' style="max-width: 100%" onclick="$(this).parent().parent().remove();"/>
                                <input type="hidden" onclick="$(this).parent().parent().remove();">
                                <span>Add photo</span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('snippet.photoselector')

@endsection