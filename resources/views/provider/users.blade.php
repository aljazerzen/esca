@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Users</div>

            @if (count($users) > 0)
                <table class="table table-stripped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>E-mail address</th>
                        <th>Address</th>
                        <th>Type</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->address }}, {{ $user->addressPostNumber }}</td>
                            <td><span class="label label-primary">{{ trans('general.user.type.' . $user->type) }}</span></td>
                            <td>
                                <a href="{{ url('provider/user/' . $user->id . '/toggletype') }}" class="btn btn-sm btn-primary">Toggle type</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="panel-body">
                    Hmmm, there are no users. Something seriously wrong must be happening...
                </div>
            @endif
        </div>
    </div>

@endsection
