@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="col-md-10 col-md-offset-1">

        @if (count($posts) > 0)
            @foreach($posts as $post)
            <div class="post">
                <div class="post-header">
                    <a href="{{ url('post/' . $post->id) }}"><h3>{{ $post->title }}</h3></a>
                </div>

                <div class="post-body">
                    {!! $post->text !!}
                </div>

                <div class="post-footer">
                    Posted on {{ $post->created_at }} by {{ $post->user->getPublicName() }}
                </div>
            </div>
            @endforeach
        @else
            <p>Hmmm, there are no posts jet.</p>
        @endif

        </div>

    </div>
@endsection

@push('styles')
<style>
    .post {
        margin-bottom: 40px;
    }

    .post-header{
        border-bottom: 1px solid #ddd;
    }

    .post-header a{
        color: #000;
    }

    .post-body {
        padding: 20px;
    }

    .post-footer{
        color: #999;
        font-size: 12px;
        border-top: 1px solid #ddd;
    }
</style>
@endpush