<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @stack('styles')

    <style>
        #section-1 {
            color: #fff;
        }
    </style>
</head>
<body id="app-layout">
<div id="section-1" style="background: url('{{ asset('img/bg1.jpg') }}') no-repeat center">
    <nav class="navbar navbar-static-top overlay-tint">
        <div class="container" id="navbar-container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/logo.png') }}" height="100"/>
                    <div class="brand-container">{{ App\Setting::get('appname') }}</div>
                </a>

            </div>

            <div class="collapse navbar-collapse">
                <!-- Left Side Of Navbar -->
                <div class="nav-left">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/posts') }}">Blog</a></li>
                        <li><a href="{{ url('/products') }}">Products</a></li>
                    </ul>
                </div>

                <div class="nav-right">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <!-- Authentication Links -->

                        @if(Auth::guest())

                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>

                        @else
                            @if(Auth::user() && Auth::user()->isProvider())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Provider panel <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/provider/posts') }}">Posts</a></li>
                                        <li><a href="{{ url('/provider/products') }}">Products</a></li>
                                        <li><a href="{{ url('/provider/deliveries') }}">Deliveries</a></li>
                                        <li><a href="{{ url('/provider/photos') }}">Photos</a></li>
                                        <li><a href="{{ url('/provider/users') }}">Users</a></li>
                                        <li><a href="{{ url('/provider/settings') }}">Settings</a></li>
                                    </ul>
                                </li>
                            @endif

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} ({{Auth::user()->getPublicName()}}) <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/requests') }}">My orders</a></li>
                                    <li><a href="{{ url('/profile') }}">Profile</a></li>
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @endif

                    </ul>

                </div>
            </div>
        </div>
    </nav>
</div>

@yield('content')

<div id="section-3">
    <div class="container footer">
        Contact us at: example@example.com +325 458 455
        <span class="pull-right">Made by someone</span>
    </div>
</div>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
        integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
@stack('scripts')
</body>
</html>
