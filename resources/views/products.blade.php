@extends('layouts.app')

@section('content')

    <div class="container">

        @if (count($products) > 0)
            @foreach($products as $product)
                <div class="col-md-4 tile-container" onclick="window.location = '{{ url("/product/$product->id") }}'">
                    <div class="tile">
                        <div class="tile-background" style="background-image: url({{ $product->thumbnail ? $product->thumbnail->url : '' }})">
                            <div class="tile-text">
                                <div class="tile-title">{{ $product->name }}</div>
                                <div class="pull-left">Price: {{ $product->pricePerUnit . "€ / " . $product->unitObj->name }}</div>
                                <div class="pull-right">{{ $product->getAvailableStock()}} available</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="panel panel-default">
                <div class="panel-heading">Empty</div>
                <div class="panel-body">
                    Hmmm, there are no products jet.
                </div>

            </div>
        @endif
    </div>


@endsection

@push('scripts')
<script>
    $().ready(function () {
        $(".product-tile .panel").each(function () {
//                 Uncomment the following if you need to make this dynamic
            var refH = $(this).height();
            var refW = $(this).width();
            var refRatio = refW / refH;


            var imgH = $(this).children("img").height();
            var imgW = $(this).children("img").width();

            if ((imgW / imgH) < refRatio) {
                $(this).addClass("portrait");
            } else {
                $(this).addClass("landscape");
            }
        })
    });
</script>
@endpush
