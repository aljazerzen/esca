
{{--
<div class="thumbnail btn-thumbnail" style="text-align: center;">
    <img src='{{ $photo->url }}' style="max-width: 100%" onclick="$(this).parent().parent().remove();"/>
    <input type="hidden" name="photo[]" value="{{ $photo->id }}"/>
    <span>{{ $photo->name }}</span>
</div>
--}}

<div class="modal fade" id="select-photo-modal" tabindex="-1" role="dialog"
     aria-labelledby="select-photo-modal-title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="select-photo-modal-title">Select photo</h4>
            </div>

            @if (\App\Photo::count() > 0)
                <table class="table table-stripped">
                    <tbody>
                    @foreach(\App\Photo::all() as $photo)
                        <tr class="photo-row" data-id="{{ $photo->id }}" data-name="{{ $photo->name }}" data-url="{{ $photo->url }}">
                            <td><img src="{{ $photo->url }}" width="300"/></td>
                            <td>{{ $photo->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

                <div class="modal-body">
                    Hmmm, there are no photos jet.
                </div>
            @endif

        </div>
    </div>
</div>

@push('scripts')
    <script>
        var caller;

        $().ready(function () {
            $('.btn-photo, .btn-thumbnail').click(function () {
                caller = $(this);
                $('#select-photo-modal').modal('show');
            });

            $('.photo-row').click(function () {
                if(caller.hasClass('btn-photo')) {
                    if(caller.find('input').val() == "") {
                        caller.parent().parent().append(caller.parent().last().clone(true));
                    }
                    caller.find('input').attr('name', 'photo[]');
                }

                $('#select-photo-modal').modal('hide');
                caller.find('input').val($(this).data('id'));
                caller.find('span').html($(this).data('name'));
                caller.find('img').attr('src', $(this).data('url'));
            });
        });
    </script>
@endpush

@push('styles')
<style>
    .btn-photo {
        height: 120px;
    }

    .thumbnail img {
        max-width: 100%;
        max-height: 80%;
    }
</style>
@endpush