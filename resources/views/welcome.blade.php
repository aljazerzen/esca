<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    @stack('styles')

    <style>
        body {
            font-family: 'Lato';
            color: #fff;
        }

        #section-1 {
            height: 1080px;
        }

        .overlay-tint {
            background-color: rgba(60, 80, 65, 0.58);
            width: 100%;
            height: 100%;
        }

        #brand-image {
            margin: 100px auto;
            display: table;
            padding: 20px 0;
        }

        .jumbotron {
            background-color: transparent;
            text-align: center;
        }

        .jumbotron h1 {
            font-size: 75px;
        }

        .jumbotron p {
            font-size: 25px;
        }

        #section-2 {
            color: #000;
        }

        #features-row > div {
            text-align: center;
            font-size: 18px;
            padding: 50px;
        }

        #section-3 {
            height: 700px;
        }

    </style>
</head>
<body id="app-layout">

<div id="section-1" style="background: url('{{ asset('img/bg1.jpg') }}') no-repeat fixed center">

    <nav class="navbar navbar-default navbar-static-top hidden">
        <div class="container" id="navbar-container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/logo.png') }}"/>
                    {{ App\Setting::get('appname') }}
                </a>

            </div>

            <div class="collapse navbar-collapse">
                <!-- Left Side Of Navbar -->
                <div class="nav-left">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/posts') }}">Posts</a></li>
                        <li><a href="{{ url('/products') }}">Products</a></li>
                    </ul>
                </div>

                <div class="nav-right">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <!-- Authentication Links -->

                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>

                    </ul>

                </div>
            </div>
        </div>
    </nav>

    <div class="overlay-tint">

        <div class="container">
            <div id="brand-image">
                <img src="{{ asset('img/logo_big.png') }}"/>
            </div>

            <div class="jumbotron">
                <h1>Esca</h1>
                <p>Online website, store and delivery sistem made for small farmers, distributing their produce.</p>
                <a href="{{ url('products') }}" class="btn btn-primary">Checkout available products</a>
                <a href="{{ url('posts') }}" class="btn btn-primary">Read our blog</a>
            </div>
        </div>
    </div>
</div>

<div id="section-2" class="container">
    <div class="jumbotron">
        <h2>We are</h2>
    </div>
    <div class="row" id="features-row">
        <div class="col-md-offset-2 col-md-3">
            <img src="{{ asset('img/icon-eco.png') }}" width="150">
            <h2>Ecological</h2>
            <p>Concerned with <b>living organisms</b>.</p>
        </div>
        <div class="col-md-offset-2 col-md-3">
            <img src="{{ asset('img/icon-local.png') }}" width="150">
            <h2>Local</h2>
            <p>Belonging or relating to a <b>particular area</b></p>
        </div>
    </div>
</div>

<div id="section-3" style="background: url('{{ asset('img/bg2.jpg') }}') no-repeat fixed top">
    <div class="overlay-tint">
        <div class="container">
            <div class="jumbotron">
                <h2>How does it work?</h2>
                <p>Simple: find your product, order and wait for delivery.</p>
            </div>
            <div class="jumbotron">
                <p>
                    Telephone: 021 54 21 45<br/>
                    Email: example@example.com<br/>
                </p>
            </div>
        </div>
    </div>
</div>


<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
        integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
@stack('scripts')
</body>
</html>
