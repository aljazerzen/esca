@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>

                @if (count($users) > 0)
                <table class="table">
                    <thead>
                        <tr><th>ID</th><th>Public name</th></tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr><td>{{ $user->id }}</td><td>{{ $user->getPublicName() }}</td></tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New post</div>

                <div class="panel-body">
                    @if($user->isProvider())
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/post') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="author" value="{{ $user->id }}"/>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-2 control-label">Title</label>

                            <div class="col-md-8">
                                <input id="title" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                            <label for="text" class="col-md-2 control-label">Text</label>

                            <div class="col-md-8">
                                <textarea id="text" class="form-control" name="text"></textarea>

                                @if ($errors->has('text'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn"></i> Post
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>

                @if (count($posts) > 0)
                    <table class="table">
                        <thead>
                            <tr><th>Title</th><th>Text</th></tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr><td>{{ $post->title }}</td><td>{{ $post->text }}</td></tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
