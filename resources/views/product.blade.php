@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <div class="col-md-6">
                <div class="tile">
                    <div class="tile-background" style="background-image: url({{ $product->thumbnail ? $product->thumbnail->url : '' }})"></div>
                </div>
            </div>
            <div class="col-md-6">
                <h1>{{ $product->name }}</h1>
                <div>Price: {{ $product->pricePerUnit . "€ / " . $product->unitObj->name }}</div>
                <div>{{ $product->getAvailableStock()}} available</div>
                <div>Last updated: {{ $product->updated_at->format(\App\Setting::get('datetime_format'))}}</div>

                <div id="description-container">
                    {{ $product->description }}
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Naroči na dom</div>
                <div class="panel-body">

                    <form role="form" method="POST" action="{{ url('/request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="product_id" value="{{ $product->id }}"/>
                        <input type="hidden" name="is_regular" id='is_regular' value="0"/>

                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount">Količina</label>

                            <div class="input-group">
                                <input id="amount" type="number" class="form-control" name="amount" min='1' max="{{ $product->getAvailableStock() }}" value="{{ old('amount') }}">
                                <span class="input-group-addon">{{ $product->unitObj->name }}</span>
                            </div>


                            @if ($errors->has('amount'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div style="height: 80px">
                            <div id="period-input-group" class="collapse">
                                <div class="form-group{{ $errors->has('period') ? ' has-error' : '' }}">
                                    <label for="period">Vsakih</label>

                                    <div class="input-group">
                                        <input id="period" type="number" class="form-control" name="period" min="1" value="{{ old('period','1') }}">
                                        <span class="input-group-addon">teden</span>
                                    </div>

                                    @if ($errors->has('period'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('period') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div id="delivery-input-group" class="collapse in">
                                <div class="form-group{{ $errors->has('delivery_id') ? ' has-error' : '' }}">
                                    <label for="amount">Dostava</label>

                                    <select id="delivery" class="form-control" name="delivery_id">
                                        @foreach(\App\Delivery::getAllOpen() as $delivery)
                                            <option value="{{ $delivery->id }}">{{ $delivery->date->format(\App\Setting::get('date_format')) }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('delivery_id'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('delivery_id') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-default" type="button" id="regular-btn">Redna dostava</button>
                            @if(Auth::user())
                                <button type="submit" class="btn btn-primary pull-right">
                                    Order
                                </button>
                            @else
                                <button type="submit" class="btn btn-primary pull-right">
                                    Login to place your order
                                </button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            @include('vendor.photogallery', ['photos' => $product->photos])
        </div>
    </div>
@endsection


@push('styles')
<style>
    #description-container {
        margin: 10px;
        padding: 10px;
        border-bottom: 1px solid #ddd;
        border-top: 1px solid #ddd;
    }
</style>
@endpush

@push('scripts')
<script>
    $().ready(function() {
        $('#regular-btn').click(function (){
            $("#period-input-group,#delivery-input-group").collapse('toggle');
            $('#is_regular').val( ($('#is_regular').val() + 1) % 2);
        });
    });

</script>
@endpush