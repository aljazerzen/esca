{{--
<div class="thumbnail btn-photo" style="text-align: center;">
    <img id='thumbnail-preview' src='{{  $product->thumbnail ? $product->thumbnail->url : ''}}' style="max-width: 100%"/>
    <input type="hidden" name="thumbnail_id" value="{{ $product->thumbnail ? $product->thumbnail->id : ''}}">
    <span>{{ $product->thumbnail ? $product->thumbnail->name : 'Select thumbnail'}}</span>
</div>
--}}
@if(count($photos) > 0)
    <div class="cd-svg-clipped-slider" data-selected="M780,0H20C8.954,0,0,8.954,0,20v760c0,11.046,8.954,20,20,20h760c11.046,0,20-8.954,20-20V20
	C800,8.954,791.046,0,780,0z" data-lateral="M795.796,389.851L410.149,4.204c-5.605-5.605-14.692-5.605-20.297,0L4.204,389.851
	c-5.605,5.605-5.605,14.692,0,20.297l385.648,385.648c5.605,5.605,14.692,5.605,20.297,0l385.648-385.648
	C801.401,404.544,801.401,395.456,795.796,389.851z">

        <div class="gallery-wrapper">
            <ul class="gallery">
                @foreach($photos as $key => $photo)
                    <li class="{{ $key == 0 ? 'selected' : ($key==1 ? 'right' : '') }}">
                        <div class="svg-wrapper">
                            <svg viewBox="0 0 800 800">
                                <title>Animated SVG</title>
                                <defs>
                                    <clipPath id="cd-image-{{ $key+1 }}">
                                        @if($key==0)
                                            <path id="cd-morphing-path-{{ $key+1 }}" d="M780,0H20C8.954,0,0,8.954,0,20v760c0,11.046,8.954,20,20,20h760c11.046,0,20-8.954,20-20V20 C800,8.954,791.046,0,780,0z"/>
                                        @else
                                            <path id="cd-morphing-path-{{ $key+1 }}" d="M795.796,389.851L410.149,4.204c-5.605-5.605-14.692-5.605-20.297,0L4.204,389.851 c-5.605,5.605-5.605,14.692,0,20.297l385.648,385.648c5.605,5.605,14.692,5.605,20.297,0l385.648-385.648 C801.401,404.544,801.401,395.456,795.796,389.851z"/>
                                        @endif
                                    </clipPath>
                                </defs>

                                <image height='800px' width="800px" clip-path="url(#cd-image-{{ $key+1 }})" xlink:href="{{ $photo->url }}"></image>
                                <use xlink:href="#cd-morphing-path-{{ $key+1 }}" class="cover-layer"/>
                            </svg>
                        </div>
                    </li>
                @endforeach

            </ul>

            <nav>
                <ul class="navigation">
                    <li><a href="#0" class="prev">Prev</a></li>
                    <li><a href="#0" class="next">Next</a></li>
                </ul>
            </nav>
        </div>

        <ul class="caption">
            @foreach($photos as $key => $photo)
                <li class="{{ $key == 0 ? 'selected' : ($key==1 ? 'right' : '') }}">{{ $photo->name }}</li>
            @endforeach
        </ul>
    </div>
@endif

@push('styles')
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/reset.css') }}">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<style>
    .cd-svg-clipped-slider .gallery li.left {
        transform: translateX(-75%) scale(0.4);
    }
</style>
@endpush

@push('scripts')
<script src="{{ asset('js/modernizr-custom.js') }}"></script>
<script src="{{ asset('js/snap.svg-min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
@endpush