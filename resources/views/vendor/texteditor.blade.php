<textarea id="{{ $name }}" name="{{ $name }}" rows="10">{{ $text }}</textarea>

@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_editor.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/css/froala_style.min.css" rel="stylesheet" type="text/css"/>
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.3.4/js/froala_editor.min.js"></script>
<script>
    $(function() {
        $('#{{ $name }}').froalaEditor({
            height: 300
        })
    });
</script>
@endpush