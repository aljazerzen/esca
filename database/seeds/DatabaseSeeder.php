<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// settings
		DB::table('settings')->delete();
		DB::table('settings')->insert(['key' => 'appname', 'value' => 'Esca']);
		DB::table('settings')->insert(['key' => 'request_regular_handle_time_before_assess', 'value' => '24']);
		DB::table('settings')->insert(['key' => 'date_format', 'value' => 'j.n.Y']);
		DB::table('settings')->insert(['key' => 'datetime_format', 'value' => 'j.n.Y H:i']);
		// $this->call(UsersTableSeeder::class);
	}
}
