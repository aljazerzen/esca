<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->string('username')->nullable();
	    	$table->string('address');
		    $table->string('addressPostNumber');
		    $table->integer('type')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('users', function (Blueprint $table) {
		    $table->dropColumn('username');
	    	$table->dropColumn('address');
		    $table->dropColumn('addressPostNumber');
		    $table->dropColumn('type');
	    });
    }
}
