<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRouteInDeliveries extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('deliveries', function (Blueprint $table) {
			$table->integer('route_id')->unsigned()->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('deliveries', function (Blueprint $table) {
			$table->integer('route_id')->unsigned()->change();
		});
	}
}
