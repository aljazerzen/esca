<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDescriptionsToText extends Migration {
	public function up() {
		Schema::table('posts', function(Blueprint $table) {
			$table->text('text')->change();
		});

		Schema::table('products', function(Blueprint $table) {
			$table->text('description')->change();
		});
	}

	public function down() {
		Schema::table('posts', function(Blueprint $table) {
			$table->string('text')->change();
		});

		Schema::table('products', function(Blueprint $table) {
			$table->string('description')->change();
		});
	}
}
