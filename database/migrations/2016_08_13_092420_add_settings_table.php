<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddSettingsTable extends Migration {
	public function up() {
		Schema::create('settings', function (Blueprint $table) {
			$table->increments('id');
			$table->string('key')->index();
			$table->string('value');
		});
	}

	public function down() {
		Schema::drop('settings');
	}
}
