<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('products', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->string('description');
		    $table->string('photo');
		    $table->decimal('pricePerUnit', 5, 2);
		    $table->integer('unit')->unsigned();
		    $table->integer('stock');
		    $table->integer('requestedStock');
		    $table->timestamps();
		    $table->foreign('unit')->references('id')->on('units');
	    });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}
}
