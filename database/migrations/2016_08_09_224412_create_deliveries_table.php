<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::enableForeignKeyConstraints();
		Schema::create('deliveries', function (Blueprint $table) {
			$table->increments('id');
			$table->date('date');
			$table->datetime('closes_at');
			$table->integer('route_id')->unsigned();
			$table->decimal('price', 5, 2);
			$table->integer('limit_type');
			$table->integer('limit');
			$table->timestamps();
			$table->foreign('route_id')->references('id')->on('routes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('deliveries');
	}
}
