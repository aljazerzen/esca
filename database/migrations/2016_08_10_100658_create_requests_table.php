<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('requests', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('delivery_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->integer('amount');
			$table->timestamps();
			$table->foreign('delivery_id')->references('id')->on('deliveries');
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('product_id')->references('id')->on('products');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('requests');
	}
}
