<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePhotoFieldInProducts extends Migration {
	public function up() {
		Schema::table('products', function (Blueprint $table) {
			$table->dropColumn('photo');
			$table->integer('thumbnail_id')->unsigned()->nullable();
			$table->foreign('thumbnail_id')->references('id')->on('photos');
		});
	}

	public function down() {
		Schema::table('products', function (Blueprint $table) {
			$table->string('photo');
			$table->dropColumn('thumbnail_id');
		});
	}
}
