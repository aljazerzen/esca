<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotosTable extends Migration {
	public function up() {
		Schema::enableForeignKeyConstraints();
		Schema::create('photos', function (Blueprint $table) {
			$table->increments('id');
			$table->string('url');
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('photo_post', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('photo_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->foreign('photo_id')->references('id')->on('photos');
			$table->foreign('post_id')->references('id')->on('posts');
		});

		Schema::create('photo_product', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('photo_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->foreign('photo_id')->references('id')->on('photos');
			$table->foreign('product_id')->references('id')->on('products');
		});
	}

	public function down() {
		Schema::drop('photos');
		Schema::drop('photo_post');
		Schema::drop('photo_product');
	}
}
