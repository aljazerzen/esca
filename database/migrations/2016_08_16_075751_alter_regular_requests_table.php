<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRegularRequestsTable extends Migration {
	public function up() {
		Schema::table('requests_regular', function (Blueprint $table) {
			$table->dropForeign('requests_regular_last_delivery_foreign');
			$table->dropColumn('last_delivery');
			$table->integer('last_delivery_id')->unsigned()->nullable();
			$table->integer('pending_request_id')->unsigned()->nullable();
			$table->boolean('has_informed_late')->default(false);
			$table->foreign('last_delivery_id')->references('id')->on('deliveries');
			$table->foreign('pending_request_id')->references('id')->on('requests');
		});
	}

	public function down() {
		Schema::table('requests_regular', function (Blueprint $table) {
			$table->dropForeign('requests_regular_last_delivery_id_foreign');
			$table->dropForeign('requests_regular_pending_request_id_foreign');
			$table->dropColumn('last_delivery_id');
			$table->dropColumn('has_informed_late');
			$table->dropColumn('pending_request_id');
			$table->integer('last_delivery')->unsigned();
			$table->foreign('last_delivery')->references('id')->on('deliveries');
		});
	}
}
