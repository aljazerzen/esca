<?php

namespace App;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Delivery extends Model {
	public static $STATUS_ACCEPTING = 0;
	public static $STATUS_CONFIRMED = 1;
	public static $STATUS_COMPLETED = 2;    // delivery was carried out
	public static $STATUS_UNMET = 3;        // limit was not reached
	public static $STATUS_REJECTED = 4;     // provider has decided he won't carry out the delivery

	public static $LIMIT_TYPE_REQUESTS = 0;
	public static $LIMIT_TYPE_PRODUCTS = 1;

	protected $table = "deliveries";

	public $fillable = ['date', 'closes_at', 'route_id', 'price', 'limit_type', 'limit'];

	public static $validationRules = [
		'date' => 'required|date_format:j.n.Y|after:closes_at',
		'closes_at' => 'required|date|after:today',
		'price' => 'required|numeric',
		'limit_type' => 'required|integer',
		'limit' => 'required|integer',
	];

	public function getDateAttribute($value) {
		return Carbon::createFromFormat('Y-m-d',$value);
	}

	public function setDateAttribute($value) {
		$this->attributes['date'] = DateTime::createFromFormat(Setting::get('date_format'), $value)->format('Y-m-d');
	}

	public function getClosesAtAttribute($value) {
		return Carbon::createFromFormat('Y-m-d H:i:s',$value);
	}

	public function setClosesAtAttribute($value) {
		$this->attributes['closes_at'] = DateTime::createFromFormat(Setting::get('datetime_format'), $value)->format('Y-m-d H:i:s');
	}

	public function requests() {
		return $this->hasMany('\App\Request');
	}

	public function setStatus($status) {
		$this->status = $status;
		$this->save();

		if($status == self::$STATUS_COMPLETED) {
			// remove sold stock
			$this->requests->each(function ($request, $key) {
				$request->product->stock -= $request->amount;
			});
		}

		if($status == self::$STATUS_CONFIRMED || $status == self::$STATUS_REJECTED) {
			// manage regular requests
			$this->requests->each(function ($request, $key) use ($status){
				if($request->regularRequest) {
					$regularRequest = $request->regularRequest;

					$regularRequest->pendingRequest()->dissociate();
					if($status == self::$STATUS_CONFIRMED) {
						$regularRequest->lastDelivery()->associate($this);
					}
					$regularRequest->save();
				};
			});
		}
	}

	public function getProgress() {
		if($this->limit_type == self::$LIMIT_TYPE_REQUESTS) {
			return $this->requests->count();
		} else if($this->limit_type == self::$LIMIT_TYPE_PRODUCTS) {
			return $this->requests->sum('amount');
		}
		return 0;
	}

	public function getPercentageProgress() {
		return round(100*$this->getProgress() / $this->limit,0);
	}

	public static function getAllOpen() {
		return self::hydrate(DB::table('deliveries')->where('closes_at', '>', new DateTime())->get());
	}

	public static function getAllToAssess() {
		return self::hydrate(DB::table('deliveries')->where('closes_at', '<=', new DateTime())->where('status', '=', self::$STATUS_ACCEPTING)->get());
	}

	public static function getAllToHandleRegular() {
		$datetime = (new Carbon())->addHours(Setting::get('request_regular_handle_time_before_assess'));
		return self::hydrate(DB::table('deliveries')->where('closes_at', '<=', $datetime)->where('status', '=', self::$STATUS_ACCEPTING)->get());
	}

	public static function getUpcomingDelivery() {
		return self::hydrate(DB::table('deliveries')->where('status', '=', self::$STATUS_ACCEPTING)->orderby('date', 'desc')->get())->first();
	}
}