<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model {
	protected $table = "requests";

	public $fillable = ['delivery_id', 'user_id', 'product_id', 'amount'];

	public static $validationRules = [
		'delivery_id' => 'required|exists:deliveries,id',
		'product_id' => 'required|exists:products,id',
		'amount' => 'required',
	];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function delivery() {
		return $this->belongsTo('App\Delivery');
	}

	public function product() {
		return $this->belongsTo('App\Product');
	}

	public function regularRequest() {
		return $this->hasOne('App\RequestRegular', 'pending_request_id');
	}

	public function fillFromRegular(RequestRegular $regular) {
		$this->user_id = $regular->user_id;
		$this->product_id = $regular->product_id;
		$this->amount = $regular->amount;
		return $this;
	}
}
