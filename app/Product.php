<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected $primaryKey = 'id';
	protected $table = "products";

	protected $fillable = ['name', 'description', 'pricePerUnit', 'unit', 'thumbnail_id'];

	public static $validationRules = [
		'name' => 'required|max:255',
		'description' => 'required',
		'thumbnail_id' => 'integer',
		'pricePerUnit' => 'required|numeric',
		'unit' => 'required|exists:units,id',
	];

	public function unitObj() {
		return $this->hasOne('App\Unit', 'id', 'unit');
	}

	public function requests() {
		return $this->hasMany('App\Request');
	}

	public function photos() {
		return $this->belongsToMany('App\Photo');
	}

	public function thumbnail() {
		return $this->belongsTo('App\Photo', 'thumbnail_id');
	}

	public function getAvailableStock() {
		return $this->stock - $this->getRequestedStock();
	}

	public function getRequestedStock() {
		return $this->requests->sum('amount');
	}

	public function canRequestStock($amount) {
		return $this->getAvailableStock() >= $amount;
	}

	public function getPercentRequested() {
		if ($this->stock <= 0) {
			return 0;
		}
		return round(100 * $this->getRequestedStock() / $this->stock, 0);
	}

	public function getPercentAvailable() {
		if ($this->stock <= 0) {
			return 0;
		}
		return round(100 * $this->getAvailableStock() / $this->stock, 0);
	}

	public function updatePhotos($photos) {
		$this->photos()->detach();
		$this->photos()->attach($photos);
		return $this;
	}
}