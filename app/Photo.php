<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {
	protected $table = "photos";

	protected static $destinationPath = 'uploads';

	public function fillFromRequest($request) {
		$this->name = $request->input('name', basename($this->photo));
		if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
			if($this->url != '') {
				// delete old photo
				unlink(self::$destinationPath.'/'.basename($this->url));
			}

			$extension = $request->file('photo')->getClientOriginalExtension();
			$fileName = sha1(time().rand()).'.'.$extension;
			$request->file('photo')->move(self::$destinationPath, $fileName);
			$this->url = asset(self::$destinationPath. '/' .$fileName);
		}
		return $this;
	}

	public function removeUpload() {
		if($this->url != '') {
			// delete old photo
			unlink(self::$destinationPath.'/'.basename($this->url));
		}
		$this->url = '';
	}

	public function delete() {
		$this->removeUpload();
		parent::delete();
	}

}
