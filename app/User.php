<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	public static $TYPE_CUSTOMER = 0;
	public static $TYPE_PROVIDER = 1;

	protected $fillable = [
		'name', 'email', 'password', 'username', 'address', 'addressPostNumber'
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	public static $validationRules = [
		'name' => 'required|max:255',
		'username' => 'max:255',
		'email' => 'required|email|max:255|unique:users,email',
		'password' => 'required|min:6|confirmed',
		'addressPostNumber' => 'digits:4',
	];

	public function validationRulesUpdate() {
		return [
			'name' => 'required|max:255',
			'username' => 'max:255',
			'email' => 'required|email|max:255|unique:users,email,' . $this->id,
			'password' => 'min:6|confirmed',
			'addressPostNumber' => 'digits:4',
		];
	}

	public static $validationRulesPasswordReset = [
		'token' => 'required',
		'email' => 'required|email',
		'password' => 'required|confirmed|min:6',
	];

	public function getPasswordAttribute($value) {
		return $value;
	}

	public function setPasswordAttribute($value) {
		$this->attributes['password'] = bcrypt($value);
	}

	public function requests() {
		return $this->hasMany('App\Request');
	}

	public function requestsRegular() {
		return $this->hasMany('App\RequestRegular');
	}

	public function getPublicName() {
		if ($this->username) {
			return $this->username;
		}
		return $this->name;
	}

	public function isProvider() {
		return $this->type == self::$TYPE_PROVIDER;
	}
}
