<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class RequestRegular extends Model {
	protected $table = "requests_regular";

	protected $fillable = ['user_id', 'product_id', 'amount', 'period'];

	public static $validationRules = [
		'product_id' => 'required|exists:products,id',
		'amount' => 'integer|required',
		'period' => 'integer|required',
	];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function lastDelivery() {
		return $this->belongsTo('App\Delivery', 'last_delivery_id');
	}

	public function pendingRequest() {
		return $this->belongsTo('App\Request', 'pending_request_id');
	}

	public function product() {
		return $this->belongsTo('App\Product');
	}

	public function needsThisDelivery(Delivery $delivery) {
		return
			!$this->lastDelivery ||
			($this->lastDelivery->status != Delivery::$STATUS_COMPLETED && $this->lastDelivery->status != Delivery::$STATUS_CONFIRMED) ||
			$this->lastDelivery->date->diffInWeeks($delivery->date) >= $this->period;
	}

	public function isLate() {
		if(!$this->lastDelivery)
			return false;
		return $this->lastDelivery->date->diffInWeeks() >= $this->period;
	}
}
