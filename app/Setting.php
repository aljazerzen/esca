<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	protected $table = 'settings';
	public $timestamps = false;

	public static function set($key, $value) {
		$setting = self::where('key',$key)->first();
		if(!$setting) {
			(new Setting(['key' => $key, 'value' => $value]))->save();
		} else {
			$setting->value = $value;
			$setting->save();
		}
	}

	public static function get($key) {
		$setting = Setting::where('key',$key)->first();
		if($setting) {
			return $setting->value;
		}
		return "";
	}
}
