<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Unit extends Model {
	protected $table = "units";

	protected $fillable = ['name', 'is_visible'];
	public $timestamps = false;

	public static $validationRules = [
		'name' => 'required',
		'is_visible' => 'boolean',
	];

	public static function getAllVisible() {
		return self::where('is_visible', 1)->get();
	}


}
