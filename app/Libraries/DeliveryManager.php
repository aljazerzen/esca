<?php

namespace App\Libraries;

use \App\Delivery;
use App\Product;
use App\Request;
use App\RequestRegular;
use Illuminate\Support\Facades\Log;

class DeliveryManager {

	public static function assessDeliveries() {
		Delivery::getAllToAssess()->each(function ($delivery, $key) {
			if ($delivery->getPercentageProgress() >= 100) {
				$delivery->setStatus(Delivery::$STATUS_CONFIRMED);
				// TODO: email provider
			} else {
				$delivery->setStatus(Delivery::$STATUS_UNMET);
				// TODO: email provider
			}
		});
	}

	public static function handleRegulars() {
		Log::info('Handling regular requests');
		$rr = RequestRegular::with('pendingRequest', 'lastDelivery')->get();

		Delivery::getAllToHandleRegular()->each(function ($delivery, $key) use ($rr) {

			$rr->each(function (RequestRegular $request, $key) use ($delivery) {

				// if regular request needs this delivery
				if ($request->needsThisDelivery($delivery)) {
//					Log::info('Regular request ' . $request->id . ' needs delivery ' . $delivery->id);

					// if there is no pending request or if regular request prefers this delivery over its current one
					if (!$request->pendingRequest || $delivery->date->lt($request->pendingRequest->delivery->date)) {

						// if there is already a pending request
						if ($request->pendingRequest) {
							// remove it
							Log::info('Regular request ' . $request->id . ' prefers delivery ' . $delivery->id . ' over ' . $request->pendingRequest->delivery_id);
							$pendingRequest = $request->pendingRequest;
							$request->pendingRequest()->dissociate();
							$request->save();
							$pendingRequest->delete();
						}

						Log::info('New request for regular request ' . $request->id . ' added');
						// add new request
						if ($request->product->canRequestStock($request->amount)) {
							$newRequest = (new Request())->fillFromRegular($request);
							$newRequest->delivery()->associate($delivery);
							$newRequest->save();
							$request->pendingRequest()->associate($newRequest);
							$request->save();
						}
					}
				}
			});

		});
		Log::info('Done handling regular requests');
	}
}