<?php

use App\Http\Middleware\ProviderMiddleware;
use App\Post;
use App\Product;
use App\Unit;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* --------[ Public ]-------- */

Route::get('/', 'PublicController@landingPage');
Route::get('/posts', 'PublicController@posts');
Route::get('/post/{post}', 'PublicController@post');
Route::get('/products' , 'PublicController@products');
Route::get('/product/{product}', 'PublicController@product');
Route::auth();

/* --------[ Customer ]--------*/
Route::group(['middleware' => 'auth'], function () {

	Route::get('/home', 'CustomerController@index');

	// profile
	Route::get('/profile', 'CustomerController@profile');
	Route::post('/profile', 'CustomerController@profileUpdate');

	// requests
	Route::get('/requests' , 'CustomerController@requests');
	Route::post('/request' , 'CustomerController@requestAdd');

	/* --------[ Provider ]-------- */

	Route::group(['middleware' => ProviderMiddleware::class], function () {

		Route::get('/provider', 'ProviderController@dashboard');

		// Posts
		Route::get('/provider/posts', 'ProviderController@posts');
		Route::get('/provider/post/{post}', 'ProviderController@post');
		Route::post('/provider/post', 'ProviderController@postAdd');
		Route::post('/provider/post/{post}', 'ProviderController@postUpdate');
		Route::get('/provider/post/{post}/delete', 'ProviderController@postDelete');

		// products
		Route::get('/provider/products' , 'ProviderController@products');
		Route::get('/provider/product/{product}' , 'ProviderController@product');
		Route::post('/provider/product' , 'ProviderController@productAdd');
		Route::post('/provider/product/{product}' , 'ProviderController@productUpdate');
		Route::post('/provider/product/{product}/stock' , 'ProviderController@productStock');
		Route::get('/provider/product/{product}/delete' , 'ProviderController@productDelete');

		// deliveries
		Route::get('/provider/deliveries', 'ProviderController@deliveries');
		Route::get('/provider/delivery/{delivery}', 'ProviderController@delivery');
		Route::post('/provider/delivery', 'ProviderController@deliveryAdd');
		Route::post('/provider/delivery/{delivery}', 'ProviderController@deliveryUpdate');
		Route::get('/provider/delivery/{delivery}/delete', 'ProviderController@deliveryDelete');
		Route::get('/provider/delivery/{delivery}/setstatus', 'ProviderController@deliverySetStatus');

		//users
		Route::get('/provider/users', 'ProviderController@users');
		Route::get('/provider/user/{user}/toggletype', 'ProviderController@userToggleType');

		//settings
		Route::get('/provider/settings', 'ProviderController@settings');
		Route::post('/provider/settings', 'ProviderController@settingsSet');
		Route::post('/provider/unit', 'ProviderController@unitAdd');
		Route::post('/provider/unit/{unit}', 'ProviderController@unitUpdate');

		// photos
		Route::get('/provider/photos', 'ProviderController@photos');
		Route::post('/provider/photo', 'ProviderController@photoAdd');
		Route::post('/provider/photo/{photo}', 'ProviderController@photoUpdate');
		Route::get('/provider/photo/{photo}/delete', 'ProviderController@photoDelete');

	});

});