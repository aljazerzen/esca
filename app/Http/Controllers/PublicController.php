<?php
/**
 * Created by PhpStorm.
 * User: aljaz
 * Date: 13. 08. 2016
 * Time: 09:13
 */

namespace App\Http\Controllers;


use App\Post;
use App\Product;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller {

	function landingPage() {
		return view('welcome');
	}

	function posts() {
		return view('posts')->with("posts", Post::getAllVisible(Auth::user()));
	}

	function post($post) {
		return view('post')->with("post", $post);
	}

	function products() {
		return view('products')->with("products", Product::all());
	}

	function product($product) {
		return view('product')->with("product", $product);
	}
}