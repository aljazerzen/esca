<?php

namespace App\Http\Controllers;

use App\Post;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller {

	public function index() {
		return view('home', ["users" => User::all(), "posts" => Post::all()]);
	}

	function profile() {
		return view('customer.profile')->with('user', Auth::user());
	}

	function profileUpdate(Request $request) {
		$this->validate($request, $request->user()->validationRulesUpdate());
		$request->user()->fill($request->all())->save();
		return redirect('/profile');
	}

	function requests() {
		return view('customer.requests');
	}

	public function requestAdd(Request $request) {
		if ($request->input('is_regular') == '0') {
			// request
			$this->validate($request, \App\Request::$validationRules);

			if (Product::find($request->input('product_id'))->canRequestStock($request->input('amount'))) {
				$req = new \App\Request($request->all());
				$req->user_id = Auth::user()->id;
				$req->save();
			}
		} else {
			// regular request
			$this->validate($request, \App\RequestRegular::$validationRules);
			$requestRegular = new \App\RequestRegular($request->all());
			$requestRegular->user_id = Auth::user()->id;
			$requestRegular->save();
		}
		return redirect('/requests');
	}
}
