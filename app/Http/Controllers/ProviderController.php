<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Photo;
use App\Post;
use App\Product;
use App\Setting;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProviderController extends Controller {

	function dashboard() {
		return view('provider/dashboard');
	}

	function posts() {
		return view('provider/posts')->with("posts", Post::all());
	}

	function post($post) {
		return view('provider/post')->with("post", $post);
	}

	function products() {
		return view('/provider/products')->with("products", Product::all());
	}

	function product($product) {
		return view('/provider/product')->with("product", $product);
	}

	function deliveries() {
		return view('provider/deliveries');
	}

	function delivery($delivery) {
		return view('provider/delivery')->with('delivery', $delivery);
	}

	function users() {
		return view('provider/users')->with('users', User::all());
	}

	public function postAdd(Request $request) {
		$this->validate($request, Post::$validationRules);
		$post = new Post($request->all());
		$post->save();
		$post->updatePhotos($request->input('photo.*'));
		return redirect('/provider/posts');
	}

	public function postUpdate(Request $request) {
		$this->validate($request, Post::$validationRules);
		$request->post->fill($request->all())->updatePhotos($request->input('photo.*'))->save();
		return redirect('/provider/posts');
	}

	public function postDelete(Request $request) {
		$request->post->delete();
		return redirect('/provider/posts');
	}

	public function unitAdd(Request $request) {
		$this->validate($request, Unit::$validationRules);
		(new Unit($request->all()))->save();
		return redirect('/provider/settings');
	}

	public function unitUpdate(Request $request) {
		$this->validate($request, Unit::$validationRules);
		$request->unit->fill($request->all())->save();
		return redirect('/provider/settings');
	}

	public function productAdd(Request $request) {
		$this->validate($request, Product::$validationRules);
		$product = new Product($request->all());
		$product->save();
		$product->updatePhotos($request->input('photo.*'));
		return redirect('/provider/products');
	}

	public function productUpdate(Request $request) {
		$this->validate($request, Product::$validationRules);
		$request->product->updatePhotos($request->input('photo.*'))->fill($request->all())->save();
		return redirect('/provider/products');
	}

	public function productDelete(Request $request) {
		$request->product->delete();
		return redirect('/provider/products');
	}

	public function productStock(Request $request) {
		$this->validate($request, ['stock' => 'required|integer']);
		$request->product->stock += $request->input('stock');
		$request->product->save();
		return redirect('/provider/products');
	}

	public function deliveryAdd(Request $request) {
		$this->validate($request, Delivery::$validationRules);
		(new Delivery($request->all()))->save();
		return redirect('/provider/deliveries');
	}

	public function deliveryUpdate(Request $request) {
		$this->validate($request, Delivery::$validationRules);
		if ($request->delivery->status != Delivery::$STATUS_ACCEPTING) {
			return redirect('/provider/deliveries')->withError("Can't change delivery any more");
		}

		$request->delivery->fill($request->all())->save();
		// TODO: inform customers
		return redirect('/provider/deliveries');
	}

	public function deliveryDelete(Request $request) {
		$request->delivery->delete();
		// TODO: inform customers
		return redirect('/provider/deliveries');
	}

	public function deliverySetStatus(Request $request) {
		$this->validate($request, ['status' => 'required|integer']);

		if ($request->delivery->status == Delivery::$STATUS_UNMET) {
			if ($request->input('status') == Delivery::$STATUS_CONFIRMED ||
				$request->input('status') == Delivery::$STATUS_REJECTED
			) {

				$request->delivery->setStatus($request->input('status'));
				// TODO inform customers that their request will be transferred to next delivery / they will have their order delivered
			} else {
				// not allowed
			}
		} elseif ($request->delivery->status == Delivery::$STATUS_CONFIRMED) {
			if ($request->input('status') == Delivery::$STATUS_COMPLETED) {
				$request->delivery->setStatus($request->input('status'));
			} else {
				// not allowed
			}
		}
		return redirect('/provider/deliveries');
	}

	public function userToggleType(Request $request) {
		if ($request->user->id == Auth::user()->id) {
			return redirect('/provider/users')->withErrors('error', "You can't change your own type");
		}
		$request->user->type = ($request->user->type == User::$TYPE_CUSTOMER ? User::$TYPE_PROVIDER : User::$TYPE_CUSTOMER);
		$request->user->save();
		return redirect('/provider/users');
	}

	public function settings() {
		return view('provider/settings');
	}

	public function settingsSet(Request $request) {
		Setting::all()->each(function ($setting, $key) use ($request){
			if($request->has($setting->key)) {
				Setting::set($setting->key, $request->input($setting->key));
			}
		});
		return redirect('provider/settings');
	}

	public function photos() {
		return view('provider/photos');
	}

	public function photoAdd(Request $request) {
		(new Photo())->fillFromRequest($request)->save();
		return redirect('provider/photos');
	}

	public function photoUpdate(Request $request) {
		$request->photo->fillFromRequest($request)->save();
		return redirect('provider/photos');
	}

	public function photoDelete(Request $request) {
		$request->photo->delete();
		return redirect('provider/photos');
	}

}
