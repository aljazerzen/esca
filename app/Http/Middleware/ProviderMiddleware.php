<?php

namespace App\Http\Middleware;

use Closure;

class ProviderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if($user = $request->user()) {
            if($user->isProvider()) {
	            return $next($request);
            }
        }
    	return redirect("/");
    }
}
