<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model {
	public static $TYPE_PUBLIC = 0;
	public static $TYPE_LOGGED_IN = 1;
	public static $TYPE_MEMBER = 2;

	protected $table = "posts";

	protected $fillable = ['title', 'text', 'author', 'type'];

	public static $validationRules = [
		'title' => 'required|max:255',
		'text' => 'required',
		'author' => 'required|integer|exists:users,id',
	];

	public function user() {
		return $this->belongsTo('App\User', 'author');
	}

	public function photos() {
		return $this->belongsToMany('App\Photo');
	}

	public function updatePhotos($photos) {
		$this->photos()->detach();
		$this->photos()->attach($photos);
		return $this;
	}

	public static function getAllVisible($user) {
		$type = 0;
		if ($user) {
			$type = 1;

			// TODO if has regular requests $type=2
			if ($user->isProvider()) {
				$type = 3;
			}
		}
		return Post::hydrate(DB::table('posts')->where('type', '<=', $type)->get());
	}
}
